# presets.py
#
# Copyright 2021 Étienne Nadji
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# ------------------------------------------------------------------------------

import json
import uuid
import copy
import collections

# pygi
from gi.repository import Gtk

from .common import MIMETYPES
from .translation import gettext as _
from .conversions import Conversion

# Preset exceptions ------------------------------------------------------------


class NoExtensions(Exception):
    """
    Exception raised when the preset has not enough file extensions defined.
    """


class Untitled(Exception):
    """Exception raised when the preset has no title."""


# Preset object ----------------------------------------------------------------


class Preset:
    """
    Conversion preset class.

    :type kwargs: dict
    :param kwargs: Kwargs (see below)

    Kwargs
    ======

    title       | Preset title
    comment     | Description of the preset
    preset_id   | Preset ID
    new_id      | Generate a random preset ID (bool)
    order       | Preset order
    loss        | Data loss of the preset ("low", "medium", "high")
    load        | Load attributes from deserialized JSON (dict)
    override_av | Override keep audio/video user choices (bool)

    Kwargs (audio parameters)
    -------------------------

    keep_audio | Keep audio data (bool)
    audio_ext  | File extension for audio files (str)
    frequency  | Frequency
    channels   | Number of audio channels
    bitrate    | Bitrate

    Kwargs (video parameters)
    -------------------------

    keep_video   | Keep video data (bool)
    video_ext    | File extension for video files (str)
    video_filter | FFmpeg video filter (list of strings)
    """

    def __init__(self, **kwargs):
        def kwargs_or(key, kwargs, default=None):
            """
            If kwargs has key, returns kwargs[key], else returns default.

            :param key: kwargs key
            :type key: str
            :param default: Default value if not kwargs[key] not found.
            """

            if key in kwargs:
                return kwargs[key]

            return default

        def kwargs_loss(kwargs):
            """
            Return standard value for data loss from kwargs.

            :rtype: str
            :returns: "low", "medium", "high"
            """

            if "loss" in kwargs:
                if kwargs["loss"] in ["low", "medium", "high"]:
                    return kwargs["loss"]

            return "high"

        self.title = kwargs_or("title", kwargs, False)
        self.comment = kwargs_or("comment", kwargs, "")
        self.preset_id = kwargs_or("preset_id", kwargs, False)

        self.loss_level = kwargs_loss(kwargs)

        self.audio = {
            # bool
            "keep": kwargs_or("keep_audio", kwargs, True),
            # int
            "frequency": kwargs_or("frequency", kwargs),
            "channels": kwargs_or("channels", kwargs),
            "bitrate": kwargs_or("bitrate", kwargs),
            # str
            "ext": kwargs_or("audio_ext", kwargs),
        }

        self.video = {
            "keep": kwargs_or("keep_video", kwargs, True),
            "scale": {"scaled": False, "width": None, "height": None},
            "filter": kwargs_or("video_filter", kwargs, []),
            "ext": kwargs_or("video_ext", kwargs),
        }

        self.override_av = kwargs_or("override_av", kwargs, False)

        self.order = kwargs_or("order", kwargs, 0)

        self.new_preset = False

        if "load" in kwargs:
            self.load(kwargs["load"])

        if "new_id" in kwargs:
            self.new_id()

    def new_id(self):
        """Sets a new, random, preset ID."""
        self.preset_id = str(uuid.uuid4())

    def get_copy(self):
        """
        Return a copy of this preset, with new ID and modified title.

        :rtype: Preset
        """

        new_preset = copy.deepcopy(self)
        new_preset.new_id()
        new_preset.new_preset = True
        new_preset.title = "{} ({})".format(self.title, _("copy"))

        return new_preset

    def get_command(self, segment):
        """
        Return command line arguments for a FFmpeg conversion according to
        preset attributes.

        :type segment: str
        :param segment: Audio/Video part of the command line to return.
            Either "audio" or "video".
        :rtype: list
        :returns: Command line arguments as a list of strings
        """

        command = []

        if segment == "audio":
            if self.audio["frequency"] is not None:
                if self.audio["frequency"]:
                    command += ["-ar", str(self.audio["frequency"])]

            if self.audio["channels"] is not None:
                if self.audio["channels"]:
                    command += ["-ac", str(self.audio["channels"])]

            if self.audio["bitrate"] is not None:
                if self.audio["bitrate"]:
                    command += ["-ab", str(self.audio["bitrate"])]

        if segment == "video":

            if self.video["scale"]["scaled"]:
                command += [
                    "-s",
                    "{}x{}".format(
                        self.video["scale"]["width"],
                        self.video["scale"]["height"],
                    ),
                ]

            if self.video["filter"] is not None:
                if self.video["filter"]:
                    command += ["-vf"] + [",".join(self.video["filter"])]

        return command

    def fromfile(self, filename):
        """
        Load the preset from a JSON file.

        :type filename: str
        :param filename: Filepath.
        """

        with open(filename, "r") as preset_file:
            self.load(json.load(preset_file))

        return True

    def tofile(self, filename):
        """
        Save the preset to a file.

        :type filename: str
        :param filename: Filepath.
        :rtype: bool
        """

        preset_id, data = self.dump()

        with open(filename, "w") as preset_file:
            preset_file.write(json.dumps(data))

        return True

    def usable(self):
        """
        Returns True if the preset is actually usable by someone, not just
        valid datas.

        :rtype: bool
        :returns: Usability
        """

        # We want titled preset
        if not self.title:
            raise Untitled()

        # At least one extension must be defined
        if self.audio["ext"] is None and self.video["ext"] is None:
            raise NoExtensions()

        return True

    def load(self, json_dict):
        """
        Load preset datas from a deserialized JSON dictionnary.

        :type json_dict: dict
        """

        self.preset_id = json_dict["id"]
        self.title = json_dict["title"]

        if "comment" in json_dict:
            self.comment = json_dict["comment"]

        self.override_av = json_dict["override_av"]

        # Loss of quality

        if json_dict["loss"] in ["low", "medium", "high"]:
            self.loss_level = json_dict["loss"]
        else:
            self.loss_level = "high"

        # Audio

        self.audio["keep"] = json_dict["audio"]["keep"]
        self.audio["ext"] = json_dict["audio"]["ext"]

        for option in ["frequency", "channels", "bitrate"]:
            if option in json_dict["audio"]:
                self.audio[option] = json_dict["audio"][option]

        # Video

        self.video["keep"] = json_dict["video"]["keep"]
        self.video["ext"] = json_dict["video"]["ext"]

        if "filter" in json_dict["video"]:
            self.video["filter"] = json_dict["video"]["filter"]

        if "scale" in json_dict["video"]:
            self.video["scale"]["scaled"] = True
            self.video["scale"]["height"] = json_dict["video"]["scale"][
                "height"
            ]
            self.video["scale"]["width"] = json_dict["video"]["scale"]["width"]

    def dump(self):
        """
        Returns everything needed for a JSON dump.

        :rtype: str, dict
        :returns: Preset ID and preset data dictionnary
        """

        data = {}

        data["id"] = self.preset_id
        data["title"] = self.title

        if self.comment:
            data["comment"] = self.comment

        data["loss"] = self.loss_level

        data["override_av"] = self.override_av

        data["video"] = {
            "ext": self.video["ext"],
            "keep": self.video["keep"],
        }

        if self.video["scale"]["scaled"]:
            data["video"]["scale"] = {
                "width": self.video["scale"]["width"],
                "height": self.video["scale"]["height"],
            }

        if self.video["filter"]:
            data["video"]["filter"] = self.video["filter"]

        data["audio"] = {"ext": self.audio["ext"], "keep": self.audio["keep"]}

        for option in ["frequency", "channels", "bitrate"]:
            if self.audio[option] is not None:
                data["audio"][option] = int(self.audio[option])

        return self.preset_id, data


@Gtk.Template(resource_path="/fr/etnadji/dispremi/preset.ui")
class PresetBox(Gtk.Box):
    """
    Preset settings in UI.
    """

    __gtype_name__ = "PresetBox"

    keep_audio = Gtk.Template.Child()
    keep_video = Gtk.Template.Child()
    override_av = Gtk.Template.Child()

    title = Gtk.Template.Child()

    video_ext = Gtk.Template.Child()
    audio_ext = Gtk.Template.Child()

    video_filter = Gtk.Template.Child()

    preset_loss = Gtk.Template.Child()
    preset_id = Gtk.Template.Child()

    ffmpeg_commands = Gtk.Template.Child()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.preset = False

    def refresh_ffmpeg_commands(self):
        """
        Refresh the FFmpeg commands used by the preset.
        """

        buffer = self.ffmpeg_commands.get_buffer()

        commands = []

        for case in [[True, True], [False, True], [True, False]]:
            conv = Conversion(
                preset=self.preset, input_file="test.avi", testing=True
            )
            conv.audio["keep"] = case[0]
            conv.video["keep"] = case[1]

            conv.set_output(frominput=True)

            command = conv.get_command()
            commands.append(" ".join(command))

            if self.preset.override_av:
                break

        buffer.set_text("\n".join(commands))

    def get_loss_level(self):
        return {0: "low", 1: "medium", 2: "high"}[
            self.preset_loss.get_active()
        ]

    def from_preset(self, preset):
        """
        Initialize UI from preset object.

        :param preset: Dispremi conversion preset
        :type preset: conversion.Preset
        """

        self.preset = preset

        self.keep_audio, self.keep_video = set_keep_checkboxes(
            self.keep_audio, self.keep_video, preset
        )
        self.title.set_text(preset.title)

        for widget, preset_data in [
            [self.video_ext, preset.video],
            [self.audio_ext, preset.audio],
        ]:
            # When the preset is a new one, exts are None
            if preset_data["ext"] is not None:
                widget.set_text(preset_data["ext"])

        self.override_av.set_state(preset.override_av)

        if preset.video["filter"]:
            self.video_filter.set_text(",".join(preset.video["filter"]))

        self.preset_id.set_text(preset.preset_id)

        qualities = [
            [0, "low", _("High")],
            [1, "medium", _("Medium")],
            [2, "high", _("Low")],
        ]

        for quality in qualities:
            self.preset_loss.insert(quality[0], quality[1], quality[2])

            if quality[1] == preset.loss_level:
                self.preset_loss.set_active_id(quality[1])

        self.refresh_ffmpeg_commands()


def set_keep_checkboxes(keep_audio, keep_video, preset):
    """
    Checks keep audio/video checkbox widgets according to a preset options.

    :type keep_audio: Gtk.CheckButton
    :param keep_audio: Keep audio checkbox widget.
    :type keep_video: Gtk.CheckButton
    :param keep_video: Keep video checkbox widget.
    :type preset: Preset
    :rtype: Gtk.CheckButton, Gtk.CheckButton
    :returns: The (not) modified widgets
    """

    for preset_data, widget in [
        [preset.audio, keep_audio],
        [preset.video, keep_video],
    ]:
        if preset_data["keep"]:
            widget.set_active(True)

    return keep_audio, keep_video


def fill_combobox(combobox, presets, default=False):
    """
    Fill a combobox with presets and set the active item as default if it
    exists.

    :param combobox: GTK combobox to fill
    :type combobox: Gtk.ComboBoxText
    :param presets: Presets to insert into combobox
    :type presets: dict
    :param default: Default preset ID to set as active combobox item.
    :type default: bool,int
    :returns: Filled combobox
    :rtype: Gtk.ComboBoxText
    """

    for preset in presets.values():
        preset_title = "{} ({})".format(
            preset.title,
            {
                "low": _("haute qualité"),
                "medium": _("moyenne qualité"),
                "high": _("basse qualité"),
            }[preset.loss_level],
        )
        combobox.insert(preset.order, preset.preset_id, preset_title)

    if default:
        combobox.set_active_id(default)

    return combobox


def update_presets_order(presets):
    """
    Update the order of the presets relatively to their order in a list.

    :param presets: List of Preset instances
    :type presets: list or collections.OrderedDict
    :returns: Updated presets list
    :rtype: list
    """

    if isinstance(presets, list):

        for idx, preset in enumerate(presets):
            preset.order = idx

    # NOTE Although OrderedDict is ordered, it is not a subclass of list, so
    # that fine to do that.
    if isinstance(presets, collections.OrderedDict):
        count = 0

        for preset_id, preset in presets.items():
            presets[preset_id].order = count
            count += 1

    return presets


def default_presets():
    """
    Return a list of default presets instances and a setting for the default
    preset to use.

    :returns: List of default presets and default preset name.
    :rtype: list,str
    """

    instances = [
        # ffmpeg [-an/-vn] INPUT OUTPUT.[webm/opus]
        Preset(
            preset_id="webm",
            title="WebM",
            loss="medium",
            audio_ext="opus",
            video_ext="webm",
        ),
        # ffmpeg [-an/-vn] INPUT OUTPUT.[mp4/m4a]
        Preset(
            preset_id="mp4",
            title="MPEG-4",
            loss="low",
            audio_ext="m4a",
            video_ext="mp4",
        ),
        # ffmpeg [-an/-vn] INPUT OUTPUT.[ogv/ogg]
        Preset(
            preset_id="ogg",
            title="Ogg Vorbis/Theora",
            loss="high",
            audio_ext="ogg",
            video_ext="ogv",
        ),
        # ffmpeg -an -i TEST.AVI -vf fps=12,scale=480:-1 TEST.gif
        Preset(
            preset_id="gif",
            title="GIF",
            loss="high",
            keep_audio=False,
            keep_video=True,
            override_av=True,
            audio_ext="gif",
            video_ext="gif",
            video_filter=["fps=12", "scale=480:-1"],
        ),
    ]

    # Default must be a preset_id in instances
    default = "webm"

    # Update presets order
    instances = update_presets_order(instances)

    # Makes a dictionnary from instances
    # presets = {}
    presets = collections.OrderedDict()

    for preset in instances:
        presets[preset.preset_id] = preset

    return presets, default

# settings.py
#
# Copyright 2021 Étienne Nadji
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Standard library

import json
import collections

from pathlib import Path

# pygi
from gi.repository import Gtk

# Dispremi
from .common import yes_no_question, error_dialog
from .translation import gettext as _
from .presets import (
    Preset,
    PresetBox,
    Untitled,
    NoExtensions,
    default_presets,
    fill_combobox,
    set_keep_checkboxes,
    update_presets_order,
)

# ------------------------------------------------------------------------------


class PresetUpdateError(Exception):
    """
    Exception raised when it is impossible to update / add preset because of
    it's attributes.
    """


class DefiningSettingsError(Exception):
    """
    Exception raised when the user defined badly a setting.
    """


class SettingsFile:
    def __init__(self, **kwargs):
        """
        Dispremi settings file.

        :param kwargs: Initialization parameters. See below.
        :type kwargs: dict

        Kwargs
        ======

        +----------+------+--------------------+-----------+
        | Key      | Type | Usage              | Mandatory |
        +----------+------+--------------------+-----------+
        | filepath | str  | Settings file path | Yes*      |
        +----------+------+--------------------+-----------+
        | new      | bool | Create a new file  | No        |
        +----------+------+--------------------+-----------+

        * filepath is mandatory even in the case of `new` parameter.
        """

        self.filepath = None

        self.presets = collections.OrderedDict()

        self.default_preset = None

        self.ux = {
            "welcome": {
                "keep": {"audio": True, "video": True},
            },
            "notification": True,
            "open-file": "xdg-open",
            "save-folder": {"same-as-source": True, "other-folder": None},
        }

        if "filepath" in kwargs:

            if "new" in kwargs:
                self.filepath = Path(kwargs["filepath"])
                self.presets, self.default_preset = default_presets()
                self.save()
            else:
                self.load(kwargs["filepath"])

    def set_default_preset(self, preset):
        """
        Set the default preset for the user.

        :param preset: Preset object or preset ID
        :type preset: dispremi.conversions.Preset, str
        """
        if isinstance(preset, Preset):
            self.default_preset = preset.preset_id
        else:
            self.default_preset = preset

    def get_default_preset(self):
        """
        Return the default preset.

        If there is no default preset defined, returns the first preset among
        the better quality available.

        :rtype: conversions.Preset
        :returns: Dispremi conversion preset
        """

        # If there is no default preset, we try to define it as the first
        # preset which has the better loss level. So it will be firs defined
        # as the first with low loss, or the first medium loss, or the first
        # high loss.

        if self.default_preset is None:
            for loss_level in ["low", "medium", "high"]:
                for preset in self.presets.values():
                    if preset.loss_level == loss_level:
                        self.default_preset = preset.preset_id
                        return preset

        # Or just returns the preset defined as default

        return self.presets[self.default_preset]

    def move_preset(self, preset_id, direction):
        """
        Change the preset matching preset_id order towards direction.

        :param preset_id: Preset ID
        :type preset_id: str
        :param direction: Move direction. Either "previous" or "next".
        :type direction: str

        :returns: Success
        :rtype: bool
        """

        to_move = preset_id
        replacement = None

        newly_ordered = collections.OrderedDict()

        # TODO Obviously repetitive, to make cleaner after

        if direction == "previous":

            # Skip moving preset if it make no sense to do it

            first = [preset.preset_id for preset in self.presets.values()][0]

            if to_move == first:
                return False

            # Get the preset to swap with

            for pid, preset in self.presets.items():
                if pid == to_move:
                    break

                replacement = pid

        if direction == "next":

            # Skip moving preset if it make no sense to do it

            last = [preset.preset_id for preset in self.presets.values()][-1]

            if to_move == last:
                return False

            # Get the preset to swap with

            for pid, preset in self.presets.items():
                if replacement == to_move:
                    replacement = pid
                    break

                replacement = pid

        # Preset to skip, to avoid duplicates when making the newly ordered
        # presets
        skip = ""

        for pid, preset in self.presets.items():
            if pid in [to_move, replacement]:
                if pid != skip:
                    # Swap presets

                    if direction == "next":
                        newly_ordered[replacement] = self.presets[replacement]
                        newly_ordered[to_move] = self.presets[to_move]
                        skip = to_move

                    if direction == "previous":
                        newly_ordered[to_move] = self.presets[to_move]
                        newly_ordered[pid] = preset
                        skip = replacement
            else:
                newly_ordered[pid] = preset

        self.presets = newly_ordered

        return True

    def save(self):
        """
        Save the settings file.
        """

        # Update the presets order
        self.presets = update_presets_order(self.presets)

        data = {}

        data["ux"] = self.ux

        data["default_preset"] = self.default_preset

        data["presets"] = {}

        for preset in self.presets.values():
            preset_id, preset_data = preset.dump()
            data["presets"][preset_id] = preset_data

        with open(str(self.filepath), "w") as stgout:
            stgout.write(json.dumps(data))

        return True

    def reset(self, group, key):
        """
        Reset something in the settings.

        :param group: Settings groups. Only "ux" now.
        :type group: str
        :param key: Setting name in the settings group.
            "open-file", "save-folder"...
        :type key: str

        :returns: True if something was done.
        :rtype: bool
        """

        if group == "ux":

            if key == "open-file":
                self.ux["open-file"] = "xdg-open"

            if key == "save-folder":
                self.ux["save-folder"] = {
                    "same-as-source": True,
                    "other-folder": None,
                }

            if key in ["open-file", "save-folder"]:
                return True

        return False

    def load(self, filepath):
        """
        Load settings from filepath.

        :param filepath: Filepath of settings file
        :type filepath: str

        :rtype: bool
        :returns: Success
        """

        path = Path(filepath)

        try:
            path.resolve(True)

            if path.is_file():

                self.filepath = path

                with open(str(path), "r") as stgsource:
                    data = json.loads(stgsource.read())

                if "ux" in data:
                    self.ux = data["ux"]

                if "default_preset" in data:
                    self.default_preset = data["default_preset"]

                if "presets" in data:
                    self.presets = data["presets"]

                    for order, preset in enumerate(data["presets"].values()):
                        new_preset = Preset(load=preset, order=order)
                        self.presets[new_preset.preset_id] = new_preset

            else:
                return False

        except FileNotFoundError:
            return False

        return True


@Gtk.Template(resource_path="/fr/etnadji/dispremi/settings.ui")
class DispremiSettings(Gtk.Dialog):
    """
    Dispremi settings dialog.
    """

    __gtype_name__ = "DispremiSettings"

    ok = Gtk.Template.Child()
    cancel = Gtk.Template.Child()
    preset_combobox = Gtk.Template.Child()

    keep_audio = Gtk.Template.Child()
    keep_video = Gtk.Template.Child()

    open_command_check = Gtk.Template.Child()
    open_command_entry = Gtk.Template.Child()
    convert_folder_button = Gtk.Template.Child()
    convert_folder_combobox = Gtk.Template.Child()

    presets_stack = Gtk.Template.Child()

    export_preset = Gtk.Template.Child()
    import_preset = Gtk.Template.Child()
    delete_preset = Gtk.Template.Child()
    reset_presets = Gtk.Template.Child()

    move_before = Gtk.Template.Child()
    move_after = Gtk.Template.Child()

    new_preset = Gtk.Template.Child()
    copy_preset = Gtk.Template.Child()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        for widget, callback in [
            # OK / Cancel
            [self.ok, self.ok_dialog],
            [self.cancel, self.cancel_dialog],
            # Presets
            [self.export_preset, self.export_preset_dialog],
            [self.import_preset, self.import_preset_dialog],
            [self.delete_preset, self.remove_preset],
            [self.new_preset, self.add_new_preset],
            [self.copy_preset, self.duplicate_preset],
            [self.reset_presets, self.restore_defaults],
            [self.move_before, self.move_previous],
            [self.move_after, self.move_next],
        ]:
            widget.connect("clicked", callback)

        self.open_command_check.connect("toggled", self.open_command_checked)
        self.open_command_entry.connect("changed", self.open_command_modified)
        self.convert_folder_combobox.connect(
            "changed", self.convert_folder_changed
        )

    def __move_preset(self, direction):
        moved = self.config.move_preset(
            self.get_current_preset().preset_id, direction
        )
        if moved:
            self._refresh_preset_boxes(True)
            # TODO Re-set the focus to the preset box matching preset_id

    def move_previous(self, button):
        self.__move_preset("previous")

    def move_next(self, button):
        self.__move_preset("next")

    def convert_folder_changed(self, combobox):
        folder_usage = self.convert_folder_combobox.get_active()
        if folder_usage:
            self.__toggle_save_folder(True)
        else:
            self.__toggle_save_folder(False)

    def open_command_checked(self, button):
        default = self.open_command_check.get_active()

        self.__toggle_open_command_check(default)

        if default:
            self.config.reset("ux", "open-file")
        else:
            self.open_command_entry.set_text("xdg-open")

    def open_command_modified(self, widget):
        text = self.open_command_entry.get_text()
        self.config.ux["open-file"] = text

    def __toggle_save_folder(self, same_folder, folder=None):
        if same_folder:
            self.convert_folder_button.set_visible(False)
        else:
            self.convert_folder_button.set_visible(True)
            self.convert_folder_button.set_filename(folder)

    def __toggle_open_command_check(self, default):
        if default:
            self.open_command_check.set_active(True)
            self.open_command_entry.set_visible(False)
        else:
            self.open_command_check.set_active(False)
            self.open_command_entry.set_visible(True)

    def init_config(self, user_config):
        self.config = user_config

        # Fill presets combobox
        self.preset_combobox = fill_combobox(
            self.preset_combobox,
            self.config.presets,
            self.config.default_preset,
        )

        # Set keep audio / video checkboxes according to default container
        self.keep_audio, self.keep_video = set_keep_checkboxes(
            self.keep_audio,
            self.keep_video,
            self.config.get_default_preset(),
        )

        # Open file with XDG or user defined application

        # Missing config fix
        if "open-file" not in self.config.ux:
            self.config.reset("ux", "open-file")

        open_default = self.config.ux["open-file"] == "xdg-open"

        self.__toggle_open_command_check(open_default)

        if not open_default:
            self.open_command_entry.set_text(self.config.ux["open-file"])

        # Save folder in the source or user defined source

        # Missing config fix
        if "save-folder" in self.config.ux:
            if not self.config.ux["save-folder"]["same-as-source"]:
                if self.config.ux["save-folder"]["other-folder"] is None:
                    self.config.reset("ux", "save-folder")
        else:
            self.config.reset("ux", "save-folder")

        use_same = self.config.ux["save-folder"]["same-as-source"]
        use_folder = self.config.ux["save-folder"]["other-folder"]

        self.convert_folder_combobox.set_active_id(
            {False: "0", True: "1"}[use_same]
        )

        self.__toggle_save_folder(use_same, use_folder)

        # Load presets GUI

        self._refresh_preset_boxes()

    def restore_defaults(self, button):
        do_restore, dialog = yes_no_question(
            self,
            _("Restoring presets to Dispremi defaults"),
            _(
                "Are you sure you want to reset your presets to those defined "
                "by Dispremi?"
            ),
        )
        dialog.destroy()

        if do_restore == "yes":
            # Restore presets the default ones defined by Dispremi
            self.config.presets, self.config.default_preset = default_presets()
            # Refresh preset boxes
            self._refresh_preset_boxes(True)

    def remove_preset(self, button):
        current_preset = self.get_current_preset()

        do_delete, dialog = yes_no_question(
            self,
            _("Deleting this preset"),
            _(
                "Are you sure you want to delete the "
                f"“{current_preset.title}”"
                " preset?"
            ),
        )
        dialog.destroy()

        if do_delete == "yes":
            # Remove the preset from the settings

            try:
                self.config.presets.pop(current_preset.preset_id)
            except KeyError:
                # If the selected preset is a new one
                pass

            # Refresh preset boxes
            self._refresh_preset_boxes(True)

    def _refresh_preset_boxes(self, erase_presets=False):
        self.presets_boxes = {}

        if erase_presets:
            for widget in self.presets_stack.get_children():
                widget.destroy()

        for preset in self.config.presets.values():
            self._add_preset_box(preset)

    def _add_preset_box(self, preset):
        widget = PresetBox()
        widget.from_preset(preset)

        self.presets_boxes[preset.preset_id] = widget

        self.presets_stack.add_titled(widget, preset.preset_id, preset.title)

    def _dialog_filters(self):
        filter_json = Gtk.FileFilter()
        filter_json.set_name(_("Presets (*.json)"))
        filter_json.add_mime_type("application/json")

        filter_any = Gtk.FileFilter()
        filter_any.set_name(_("Any files"))
        filter_any.add_pattern("*")

        return [filter_json, filter_any]

    def import_preset_dialog(self, button):
        dialog = Gtk.FileChooserNative(
            title=_("Import a preset"),
            action=Gtk.FileChooserAction.OPEN,
        )

        for dialog_filter in self._dialog_filters():
            dialog.add_filter(dialog_filter)

        response = dialog.run()

        if response == Gtk.ResponseType.ACCEPT:
            filename = dialog.get_filename()

            new_preset = Preset()
            new_preset.fromfile(filename)

            replacing = new_preset.preset_id in self.config.presets

            # NOTE When the preset_id already exists, the old preset in the
            # config is replaced with the new one.
            self.config.presets[new_preset.preset_id] = new_preset

            if replacing:
                # TODO Recreate all preset boxes
                pass
            else:
                # Insert the new preset
                self._add_preset_box(new_preset)

        if response == Gtk.ResponseType.CANCEL:
            pass

        dialog.destroy()

    def get_current_preset_box(self):
        return self.presets_stack.get_visible_child()

    def get_current_preset(self):
        return self.get_current_preset_box().preset

    def export_preset_dialog(self, button):
        dialog = Gtk.FileChooserNative(
            title=_("Export a preset"),
            action=Gtk.FileChooserAction.SAVE,
        )

        for dialog_filter in self._dialog_filters():
            dialog.add_filter(dialog_filter)

        response = dialog.run()

        if response == Gtk.ResponseType.ACCEPT:
            filename = dialog.get_filename()
            current_preset = self.get_current_preset()
            current_preset.tofile(filename)

        if response == Gtk.ResponseType.CANCEL:
            pass

        dialog.destroy()

    def ok_dialog(self, button):
        error_raised = False

        # Get & set the default preset from the combobox -----------------------

        preset = self.preset_combobox.get_active()

        for container in self.config.presets.values():
            if container.order == preset:
                preset = container
                break

        self.config.set_default_preset(preset)

        # Default application to open file -------------------------------------

        text = self.open_command_entry.get_text()

        if not text:
            if not self.config.ux["open-file"]:
                self.config.reset("ux", "open-file")

        # Folder to save converted file ----------------------------------------

        folder_usage = self.convert_folder_combobox.get_active()

        if folder_usage:
            # The same folder
            self.config.reset("ux", "save-folder")
        else:
            # An user defined folder
            folder = self.convert_folder_button.get_filename()

            if folder is None:
                # The user has not selected a single folder.
                dialog = error_dialog(
                    self,
                    _("Single folder for converted files"),
                    _(
                        f"The single folder for converted files has not been "
                        "set. Please set it or revert to the usage of the same "
                        "folder than the source."
                    ),
                )
                dialog.destroy()

                error_raised = True

            # self.config.reset("ux", "save-folder")

            self.config.ux["save-folder"] = {
                "same-as-source": False,
                "other-folder": folder,
            }

        # Keep audio / video ---------------------------------------------------

        keep_audio = self.keep_audio.get_active()
        keep_video = self.keep_video.get_active()

        self.config.ux["welcome"]["keep"]["audio"] = keep_audio
        self.config.ux["welcome"]["keep"]["video"] = keep_video

        # Update the presets ---------------------------------------------------

        self.update_presets()

        # Save the presets and configuration or fail to close dialog -----------

        try:
            if error_raised:
                raise DefiningSettingsError()

            self.save_new_presets()
            self.config.save()
            self.close()
        except PresetUpdateError:
            # Don't save and close the settings window if it is impossible
            # to update / add presets.
            pass
        except DefiningSettingsError:
            # Idem as above
            pass

    def duplicate_preset(self, button):
        current_preset = self.get_current_preset()
        new_preset = current_preset.get_copy()
        self._add_preset_box(new_preset)

    def add_new_preset(self, button):
        # The new preset is defined by default as a preset with high data loss
        # and no audio/video override

        new_preset = Preset(
            new_id=True, title=_("New preset"), override_av=False, loss="high"
        )

        # Marks the preset as a new one to find it later
        new_preset.new_preset = True

        self._add_preset_box(new_preset)

    def save_new_presets(self):
        """
        Search for new presets and add them to the configuration.
        """

        for preset_id, widget in self.presets_boxes.items():
            if preset_id not in self.config.presets:
                # Quickly update some informations because they are needed
                # to check if the preset is usable, but we update the presets
                # from the widget only if it is usable…

                widget.preset.title = widget.title.get_text()
                widget.preset.audio["ext"] = widget.audio_ext.get_text()
                widget.preset.video["ext"] = widget.video_ext.get_text()

                # If the preset has enough information to be usefull
                try:
                    if widget.preset.usable():
                        widget.preset, widget = self._update_from_widget(
                            widget.preset, widget
                        )
                        self.config.presets[preset_id] = widget.preset

                except Untitled:
                    dialog = error_dialog(
                        self, _("Untitled preset"), _("A preset has no title.")
                    )
                    dialog.destroy()

                    raise PresetUpdateError()

                except NoExtensions:
                    dialog = error_dialog(
                        self,
                        _("Preset without file extensions"),
                        _(
                            f"The preset {widget.preset.title} doesn't define any extension for the conversion output files."
                        ),
                    )
                    dialog.destroy()

                    raise PresetUpdateError()

        return True

    def _update_from_widget(self, preset, widget):
        # Get all new options from the preset widget --------------------

        pr = {}

        pr["title"] = widget.title.get_text()
        pr["video-ext"] = widget.video_ext.get_text()
        pr["audio-ext"] = widget.audio_ext.get_text()

        pr["keep-audio"] = widget.keep_audio.get_active()
        pr["keep-video"] = widget.keep_video.get_active()

        pr["override-user-av"] = widget.override_av.get_state()

        pr["video-filter"] = widget.video_filter.get_text()

        pr["loss"] = widget.get_loss_level()

        # Update the preset ---------------------------------------------

        preset.title = pr["title"]

        # Extensions
        preset.video["ext"] = pr["video-ext"]
        preset.audio["ext"] = pr["audio-ext"]

        # Keep audio / video
        preset.video["keep"] = pr["keep-video"]
        preset.audio["keep"] = pr["keep-audio"]

        # Override AV
        preset.override_av = pr["override-user-av"]

        # loss
        preset.loss_level = pr["loss"]

        # Video filter
        if pr["video-filter"]:
            preset.video["filter"] = pr["video-filter"].split(",")
        else:
            preset.video["filter"] = []

        return preset, widget

    def update_presets(self):
        """
        Update presets from the existing PresetBox(es).
        """

        for preset in self.config.presets.values():
            widget = self.presets_boxes[preset.preset_id]
            self._update_from_widget(preset, widget)

    def cancel_dialog(self, button):
        """Close the dialog"""
        self.close()


# ------------------------------------------------------------------------------

# common.py
#
# Copyright 2021 Étienne Nadji
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Common functions and values used in Dispremi parts.
"""

# pygi
from gi.repository import Gtk

# NOTE No, I didn't test all of those 52 mimetypes

MIMETYPES = [
    "video/x-mpeg",
    "video/msvideo",
    "video/quicktime",
    "video/x-avi",
    "video/x-ms-asf",
    "video/x-ms-wmv",
    "video/x-msvideo",
    "video/x-nsv",
    "video/x-flc",
    "video/x-fli",
    "video/x-flv",
    "video/vnd.rn-realvideo",
    "video/mp4",
    "video/mp4v-es",
    "application/ogg",
    "application/x-ogg",
    "video/x-ogm+ogg",
    "audio/x-vorbis+ogg",
    "application/x-matroska",
    "audio/x-matroska",
    "video/x-matroska",
    "video/webm",
    "audio/webm",
    "audio/x-mp3",
    "audio/x-mpeg",
    "audio/mpeg",
    "audio/x-wav",
    "audio/x-mpegurl",
    "audio/x-scpls",
    "audio/x-m4a",
    "audio/x-ms-asf",
    "audio/x-ms-asx",
    "audio/x-ms-wax",
    "application/vnd.rn-realmedia",
    "audio/x-real-audio",
    "audio/x-pn-realaudio",
    "application/x-flac",
    "audio/x-flac",
    "audio/vnd.rn-realaudio",
    "audio/x-pn-aiff",
    "audio/x-pn-au",
    "audio/x-pn-wav",
    "audio/x-pn-windows-acm",
    "application/x-extension-mp4",
    "audio/mp4",
    "audio/amr",
    "audio/amr-wb",
    "application/x-shorten",
    "audio/x-ape",
    "audio/x-wavpack",
    "audio/x-tta",
    "audio/x-opus+ogg",
]


def escaped(thing, quote="double"):
    """
    Escape things as strings, using single or double quotes.

    :param thing: Value to escape
    :type quote: str
    :param quote: Wether Use double or single quotes.
        'double' or 'single'
    :rtype: str
    :returns: Escaped value
    :raises: ValueError
    """

    if quote.lower() in ["double", "single"]:
        if quote == "double":
            return '"' + str(thing) + '"'
        if quote == "single":
            return "'" + str(thing) + "'"

    raise ValueError("Invalid parameter for escaped function")


def __message_dialog(
    parent, message_type, buttons, text, secondary_text=False
):
    """
    Make a GTK message dialog.

    :type parent: Gtk.ApplicationWindow
    :type message_type: Gtk.MessageType
    :type buttons: Gtk.ButtonsType
    :type text: str
    :type secondary_text: str,bool

    :rtype: Gtk.MessageDialog
    :returns: GTK Message dialog
    """

    dialog = Gtk.MessageDialog(
        transient_for=parent,
        flags=0,
        message_type=message_type,
        buttons=buttons,
        text=text,
    )

    if secondary_text:
        dialog.format_secondary_text(secondary_text)

    return dialog


def yes_no_question(parent, text, secondary_text=False):
    """
    Make a GTK message dialog with a yes/no choice.

    :type parent: Gtk.ApplicationWindow
    :type text: str
    :type secondary_text: str,bool

    :rtype: str,Gtk.MessageDialog
    :returns: User choice ("yes", "no") and GTK Message dialog
    """

    dialog = __message_dialog(
        parent,
        Gtk.MessageType.QUESTION,
        Gtk.ButtonsType.YES_NO,
        text,
        secondary_text,
    )

    response = dialog.run()

    if response == Gtk.ResponseType.YES:
        return "yes", dialog

    if response == Gtk.ResponseType.NO:
        return "no", dialog

    return None, dialog


def info_dialog(parent, text, secondary_text=False):
    """
    Returns an info dialog with message and secondary texts.

    :param text: Message text
    :param secondary_text: Secondary message text or False
    :type text: str
    :type secondary_text: str,bool

    :rtype: Gtk.MessageDialog
    :returns: GTK Message dialog
    """

    dialog = __message_dialog(
        parent, Gtk.MessageType.INFO, Gtk.ButtonsType.OK, secondary_text
    )

    response = dialog.run()

    return dialog


def error_dialog(parent, text, secondary_text=False):
    """
    Returns an error dialog with message and secondary texts.

    :param text: Message text
    :param secondary_text: Secondary message text or False
    :type text: str
    :type secondary_text: str, bool

    :rtype: Gtk.MessageDialog
    :returns: GTK Message dialog
    """

    dialog = __message_dialog(
        parent, Gtk.MessageType.ERROR, Gtk.ButtonsType.OK, text, secondary_text
    )

    response = dialog.run()

    return dialog

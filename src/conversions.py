# conversions.py
#
# Copyright 2021 Étienne Nadji
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# ------------------------------------------------------------------------------

from pathlib import Path

from .translation import gettext as _
from .common import escaped

# Exceptions -------------------------------------------------------------------


class NoConversionOutput(Exception):
    pass


class NoConversionInput(Exception):
    pass


class NothingToConvert(Exception):
    pass


# Conversion settings object ---------------------------------------------------


class Conversion:
    def __init__(self, **kwargs):
        self.input = None

        self.output = {
            "file": None,
            "folder": None,
        }

        self.audio = {"keep": True}
        self.video = {"keep": True}

        # Reading options

        if "preset" in kwargs:
            self.preset = kwargs["preset"]

        if self.preset.override_av:
            self.audio["keep"] = self.preset.audio["keep"]
            self.video["keep"] = self.preset.video["keep"]
        else:
            if "keep_audio" in kwargs:
                self.audio["keep"] = kwargs["keep_audio"]

            if "keep_video" in kwargs:
                self.video["keep"] = kwargs["keep_video"]

        # For cases where we don't want to convert anything but test the
        # resulting FFmpeg command

        testing = False

        if "testing" in kwargs:
            testing = kwargs["testing"]

        # If the user has defined a folder for the converted files

        if "other_folder" in kwargs:
            if kwargs["other_folder"]:
                out_folder = kwargs["other_folder"]
            else:
                out_folder = False
        else:
            out_folder = False

        # And finally set the input file

        if "input_file" in kwargs:
            self.set_input(kwargs["input_file"], testing, out_folder)

    def set_input(self, path, testing=False, other_folder=False):
        """
        Set the input file of the conversion and checks if it exists unless
        testing is True.

        :param path: Input filepath
        :type path: str
        :param testing: Do not performs file tests
        :type testing: bool
        """
        self.input = Path(path)

        try:
            self.input.resolve(True)

            if not self.input.is_file():
                if not testing:
                    self.input = None

            if self.input is not None:
                self.set_output(frominput=True, other_folder=other_folder)

        except FileNotFoundError:
            if not testing:
                self.input = None

    def get_command(self, joined=False):
        """
        Returns the FFmpeg command arguments list according to the preset and
        user choices.

        :rtype: list
        """

        if self.input is None:
            raise NoConversionInput()

        command = ["ffmpeg", "-hide_banner", "-y", "-i"]
        command += [escaped(self.input, "double")]

        # Override AV. These lines are only usefull when the conversion
        # has not been initialized with a preset
        if self.preset.override_av:
            self.audio["keep"] = self.preset.audio["keep"]
            self.video["keep"] = self.preset.video["keep"]

        # Remove audio / video if needed

        if self.nothing_to_convert():
            # We cant remove both audio and video !
            raise NothingToConvert()
        else:
            if not self.audio["keep"]:
                command += ["-an"]

            if not self.video["keep"]:
                command += ["-vn"]

        # Get commands parts from the preset if they are needed

        if self.video["keep"]:
            command += self.preset.get_command("video")

        if self.audio["keep"]:
            command += self.preset.get_command("audio")

        # Add the output file part of the command

        if self.output["file"]:
            command += [escaped(self.output["file"], "double")]

        if joined:
            return " ".join(command)

        return command

    def set_output(self, **kwargs):
        """
        Define the output file of the conversion.

        :param kwargs: Kwargs options
        :type kwargs: dict

        frominput : Use the input file path to deduce the output file path
            Ex: /home/user/test.avi => /home/user/test.ogg

        filepath: Use the value as the output filepath

        other_folder: Same as frominput, but use an arbitrary folder instead.
            To be used with frominput.
            Ex: /home/user/test.avi => /home/user/Dispremi/test.ogg
        """

        if "frominput" in kwargs:

            self.output["folder"] = self.input.parent

            if self.preset:

                if self.audio["keep"] and self.video["keep"]:
                    extkey = "video"
                else:
                    if self.nothing_to_convert():
                        # We cant remove both audio and video !
                        raise NothingToConvert()

                    if not self.video["keep"]:
                        extkey = "audio"

                    if not self.audio["keep"]:
                        extkey = "video"

                if extkey == "video":
                    ext = self.preset.video["ext"]

                if extkey == "audio":
                    ext = self.preset.audio["ext"]

                if "other_folder" in kwargs:
                    if kwargs["other_folder"]:
                        out_folder = Path(kwargs["other_folder"])
                    else:
                        out_folder = self.output["folder"]
                else:
                    out_folder = self.output["folder"]

                self.output["file"] = out_folder / "{}.{}".format(
                    self.input.stem, ext
                )

        if "filepath" in kwargs:
            self.output["file"] = kwargs["filepath"]

    def nothing_to_convert(self):
        """
        Check if there is not audio and video to keep.
        """
        return (
            self.audio["keep"] == self.video["keep"] and not self.audio["keep"]
        )

    def ready(self):
        """
        Raises appropriate exception if the conversion is not ready or just
        returns True.

        :rtype: bool
        """
        if self.input is None:
            raise NoConversionInput()

        if self.output["file"] is None:
            raise NoConversionOutput()

        if self.nothing_to_convert():
            raise NothingToConvert()

        return True

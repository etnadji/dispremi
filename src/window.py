# window.py
#
# Copyright 2021 Étienne Nadji
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Standard library

import time
import subprocess
import mimetypes

import os
from pathlib import Path

# from gettext import gettext as _

# pygi
from gi.repository import Gtk, GLib

# Dispremi
from .common import MIMETYPES, error_dialog
from .conversions import (
    Conversion,
    NoConversionInput,
    NoConversionOutput,
    NothingToConvert,
)
from .presets import fill_combobox, set_keep_checkboxes
from .settings import DispremiSettings
from .translation import gettext as _

# ------------------------------------------------------------------------------


@Gtk.Template(resource_path="/fr/etnadji/dispremi/window.ui")
class DispremiWindow(Gtk.ApplicationWindow):
    __gtype_name__ = "DispremiWindow"

    # Dispremi window main components ----------------

    app_stack = Gtk.Template.Child()
    header_bar = Gtk.Template.Child()

    # Welcome screen ---------------------------------

    video_file = Gtk.Template.Child()
    keep_audio = Gtk.Template.Child()
    keep_video = Gtk.Template.Child()
    do_conversion = Gtk.Template.Child()
    preset_combobox = Gtk.Template.Child()

    # Conversion success screen ----------------------

    see_converted = Gtk.Template.Child()
    new_conversion = Gtk.Template.Child()
    open_converted = Gtk.Template.Child()
    next_conversion = Gtk.Template.Child()
    next_conversion_box = Gtk.Template.Child()

    # Conversion fail screen -------------------------

    fail_new_conversion = Gtk.Template.Child()

    # Application menu -------------------------------

    app_quit = Gtk.Template.Child()
    app_about = Gtk.Template.Child()
    app_settings = Gtk.Template.Child()

    def __init__(self, **kwargs):
        """
        Dispremi main window.
        """

        super().__init__(**kwargs)

        self.application = kwargs["application"]

        self.see_converted.connect("clicked", self.see_converted_file)
        self.open_converted.connect("clicked", self.open_converted_file)
        self.new_conversion.connect("clicked", self.go_welcome)
        self.next_conversion.connect("clicked", self.convert_next)

        self.fail_new_conversion.connect("clicked", self.go_welcome)

        self.keep_audio.connect("toggled", self.toggle_keep, "audio")
        self.keep_video.connect("toggled", self.toggle_keep, "video")

        self.app_about.connect("activate", self.about)
        self.app_quit.connect("activate", self.quit)
        self.app_settings.connect("activate", self.settings)

        self.conversion = None

        self.from_cli = False

        # Switch to Welcome page
        self._welcome_stack()

    def init_config(self, config, logger):
        """
        Initialize Dispremi configuration and logger.

        :param config: Dispremi configuration file
        :type config: dispremi.settings.SettingsFile
        :param logger: Logger instance
        :type logger: logging.Logger
        """

        self.config = config
        self.logger = logger

        # Set checkboxes state
        for widget, key in [
            [self.keep_audio, "audio"],
            [self.keep_video, "video"],
        ]:
            widget.set_active(self.config.ux["welcome"]["keep"][key])

        self._refresh_welcome()

    def _refresh_welcome(self, erase_presets=False):
        self._refresh_presets_combobox(erase_presets)
        self._refresh_audio_video_checkboxes()

    def _refresh_presets_combobox(self, erase_all=False):
        if erase_all:
            self.preset_combobox.remove_all()

        # Try to fix invalid default preset ID
        if isinstance(self.config.default_preset, int):
            self.config.default_preset = [
                preset.preset_id for preset in self.config.presets.values()
            ][0]

        # Fill presets combobox
        self.preset_combobox = fill_combobox(
            self.preset_combobox,
            self.config.presets,
            self.config.default_preset,
        )

    def _refresh_audio_video_checkboxes(self):
        """Set keep audio / video checkboxes according to default container."""

        self.keep_audio, self.keep_video = set_keep_checkboxes(
            self.keep_audio, self.keep_video, self.config.get_default_preset()
        )

    def init_cli(self, filepath=False):
        if filepath:
            # Try to load the file in CLI arguments

            filepath = Path(filepath)

            if filepath.exists():
                self.from_cli = str(filepath)
                self.video_file.set_filename(str(filepath))
                self.do_conversion.connect("clicked", self.pre_convert)
            else:
                self._nocli()
        else:
            self._nocli()

    def quit(self, button):
        """
        (Callback)

        Quit Dispremi.

        :param button: GTK button
        :type button: Gtk.Button
        """

        self.destroy()
        return True

    def settings(self, button=False):
        """
        (Callback)

        Open the settings dialog.

        :param button: GTK button
        :type button: Gtk.Button
        """

        prefs = DispremiSettings()
        prefs.init_config(self.config)

        prefs.run()
        prefs.destroy()

        self._refresh_welcome(erase_presets=True)

    def about(self, button):
        """
        (Callback)

        Show About dialog.

        :param button: GTK button
        :type button: Gtk.Button
        """

        comment = _(
            """Simple one-file FFmpeg convertor.

Thanks to all FFmpeg contributors !"""
        )

        about = Gtk.AboutDialog()
        about.set_transient_for(self)
        about.set_version("2021.08.07")
        about.set_program_name("Dispremi")
        about.set_title("Dispremi")
        about.set_license_type(Gtk.License.GPL_3_0)
        about.set_comments(comment)
        about.set_copyright("Copyright © 2021 Étienne Nadji")
        about.set_authors(
            [
                "Étienne Nadji (code)",
                "ÉN's mother (original need)",
                # The translation.py file used for localisation is from the
                # Portfolio application : https://github.com/tchx84/Portfolio
                "Clayton Craft (i18n code from Portfolio)",
                "Martin Abente Lahaye (i18n code from Portfolio)",
                "Celluloid / GNOME MPV (icon)",
            ]
        )
        about.set_logo_icon_name("fr.etnadji.dispremi")
        about.set_website("https://etnadji.fr/rsc/dispremi")
        about.run()
        about.destroy()

    def toggle_keep(self, button, param):
        """
        (Callback)

        Prevents the options to remove both video and audio.

        :param button: GTK button
        :type button: Gtk.Button
        """

        current_state = button.get_active()

        if param == "audio":
            if not current_state:
                if not self.keep_video.get_active():
                    self.keep_video.set_active(True)

        if param == "video":
            if not current_state:
                if not self.keep_audio.get_active():
                    self.keep_audio.set_active(True)

    def pre_convert(self, param=False):
        """
        Prepare the conversion, handle errors, and starts conversion if
        everything is fine.
        """

        # Get the preset from the combobox -------------------------------------

        preset = self.preset_combobox.get_active()

        for container in self.config.presets.values():
            if container.order == preset:
                preset = container
                break

        if isinstance(preset, int):
            self.logger.warning(
                f"Dispremi failed to found preset {preset} in :"
            )

            for preset in self.config.presets.values():
                self.logger.warning(f"{preset.order} - {preset.title}")

            return False

        # Initialize the conversion settings object ----------------------------

        if self.from_cli:
            filename = self.from_cli
        else:
            filename = self.video_file.get_filename()

        if self.config.ux["save-folder"]["same-as-source"]:
            other_folder = False
        else:
            # If the configuration is half complete
            if self.config.ux["save-folder"]["other-folder"] is None:
                other_folder = False
            else:
                other_folder = self.config.ux["save-folder"]["other-folder"]

        new_conversion = Conversion(
            input_file=filename,
            keep_audio=self.keep_audio.get_active(),
            keep_video=self.keep_video.get_active(),
            preset=preset,
            other_folder=other_folder,
        )

        # Checks if the conversion settings are ok and converts if everything
        # is fine

        error = {}

        try:

            # Starts the conversion --------------------------------------------

            if new_conversion.ready():
                self.app_stack.set_visible_child_name("converting")
                self.header_bar.set_subtitle(str(new_conversion.input.name))
                self.convert(new_conversion)

            # Errors raised ----------------------------------------------------

        except NoConversionInput:
            # The input file doesn't exists
            error["message"] = _("The file to convert doesn't exists.")

        except NothingToConvert:
            # Somehow the user try to convert to a file that will have no
            # audio AND no video
            error["message"] = _("There is nothing to convert.")
            error["secondary"] = _(
                "You must keep at least the audio or the video in the "
                "converted file."
            )

        except NoConversionOutput:
            # The conversion output is not set.
            error["message"] = _("The output file is not defined.")
            error["secondary"] = _(
                "This is not normal. Please report this as a bug."
            )

        # Show message dialog for the error raised -----------------------------

        if error:
            if "secondary" in error:
                dialog = error_dialog(
                    self, error["message"], error["secondary"]
                )
            else:
                dialog = error_dialog(self, error["message"])

            dialog.destroy()

    def _nocli(self):
        """Configure the window as it is no file selected with CLI argument."""

        self.from_cli = False

        # If we connect file-set when Dispremi has a file as CLI argument,
        # the user will not be able to select options in the Welcome page
        self.video_file.connect("file-set", self.pre_convert)
        self.do_conversion.set_visible(False)

    def go_welcome(self, button=False):
        """Resets things and go the Welcome page."""

        # If Dispremi has been started from CLI, the file-set signal has not
        # been set, so we need to set it, or the video_file button will do
        # nothing
        if self.from_cli:
            self._nocli()

        # Reset informations about the current conversion
        self.conversion = None
        self.video_file.unselect_all()

        # Switch to Welcome page
        self._welcome_stack()

    def _welcome_stack(self):
        """Switch to the welcome page, focus the file chooser widget."""

        self.header_bar.set_subtitle("")
        self.app_stack.set_visible_child_name("welcome")
        self.video_file.grab_focus()

    def __check_open_command(self):
        """
        Check if open command parameters are fine before using those parameters.
        """

        reset_open = False

        if "open-file" in self.config.ux:
            if not self.config.ux["open-file"]:
                reset_open = True
        else:
            reset_open = True

        if reset_open:
            self.config.ux["open-file"] = "xdg-open"

    def see_converted_file(self, button=False, param=False):
        """
        (Callback)

        Open a file explorer at the location of the latest converted file.

        :param button: GTK button
        :type button: Gtk.Button
        """

        if self.conversion is not None:

            if self.config.ux["save-folder"]["same-as-source"]:
                folder = self.conversion.output["folder"]
            else:
                # If the configuration is half complete
                if self.config.ux["save-folder"]["other-folder"] is None:
                    folder = self.conversion.output["folder"]
                else:
                    folder = self.config.ux["save-folder"]["other-folder"]

            subprocess.Popen(["xdg-open", folder])

    def open_converted_file(self, button=False, param=False):
        """
        (Callback)

        Open the latest converted file.

        :param button: GTK button
        :type button: Gtk.Button
        """

        if self.conversion is not None:
            converted_file = self.conversion.output["file"]
            self.__check_open_command()
            subprocess.Popen(["xdg-open", converted_file])

    def convert_next(self, button):
        """
        (Callback)

        Start the conversion of the next file, from previous conversion success
        screen.

        :param button: GTK button
        :type button: Gtk.Button
        """

        self.video_file.set_filename(str(button.input_file))
        self.pre_convert()

    def go_success(self):
        """
        Switch to the success page and sends a notification.
        """

        self.app_stack.set_visible_child_name("success")

        self.header_bar.set_subtitle(str(self.conversion.output["file"].name))

        # End of conversion notification ---------------------------------------

        if "notification" in self.config.ux:
            if self.config.ux["notification"]:
                output = self.conversion.output["file"].name

                self.application.do_notification(
                    _("Conversion finished"),
                    _(f"{output} is ready !"),
                    actions=[
                        [_("Play"), "app.open-converted"],
                        [_("Folder"), "app.see-converted"],
                    ],
                )

        # Finds the next possible conversion -----------------------------------

        possible_conversions = [
            nc for nc in self.conversion.input.parent.iterdir() if nc.is_file()
        ]

        mime_filtered = []

        for possible in possible_conversions:
            guess = mimetypes.guess_type(str(possible))

            # We keep the already converted file because we want to select
            # the next file in alphanumeric order (so we need a start index).
            if possible == self.conversion.output["file"]:
                mime_filtered.append(possible)
                continue

            # If possible file have a supported mimetypes
            if guess[0] is not None:
                if guess[0] in MIMETYPES:
                    mime_filtered.append(possible)
                    continue

            # Sometimes mimetypes.guess_type fail to recognize mimetypes,
            # especially with files having data containers extensions like
            # matroska
            for extension in ["mkv"]:
                if possible.suffix == f"{extension}":
                    mime_filtered.append(possible)
                    break

        possible_conversions = sorted(mime_filtered)

        if len(possible_conversions) > 1:
            # Get the next conversion file as it follow
            # self.conversion.output["file"] (same folder) OR
            # self.conversion.input (origin folder)

            if self.config.ux["save-folder"]["same-as-source"]:
                next_conv_origin = self.conversion.output["file"]
            else:
                next_conv_origin = self.conversion.input

            try:
                next_conv = (
                    possible_conversions.index(str(next_conv_origin)) + 1
                )
            except ValueError:
                next_conv = False

            try:
                next_conv = possible_conversions[next_conv]
            except IndexError:
                next_conv = False

        else:
            next_conv = False

        # Show (or not) a button to start the next_conversion ------------------

        if next_conv:
            self.next_conversion_box.set_visible(True)
            self.next_conversion.set_label(str(next_conv.name))
            self.next_conversion.input_file = next_conv
        else:
            self.next_conversion_box.set_visible(False)

        # ----------------------------------------------------------------------

    def __convert_output(self, src, cond):
        """
        Watch method for conversion process.

        :returns: True when processus finished.
        :rtype: bool
        """

        Gtk.main_iteration()

        if cond == GLib.IO_IN:
            # When the process is running
            return True

        if cond == GLib.IO_HUP:
            return False

        return False

    def __convert_end(self, pid, err_code):
        """
        End method for conversion process.

        :param pid: Process PID
        :param error_code: Process error return code
        :type error_code: int
        """

        if err_code == 0:
            # Switch to conversion success screen
            self.go_success()
        else:
            # Switch to conversion failure screen
            self.app_stack.set_visible_child_name("fail")

            command = self.conversion.get_command(True)
            self.logger.warning("Conversion failed.")
            self.logger.warning(f"FFmpeg command : {command}")

    def convert(self, conversion):
        """
        Do a conversion according to the conversion object.

        :param conversion: Conversion settings object
        :type: conversions.Conversion
        """

        # ----------------------------------------------------------------

        command = conversion.get_command(True)
        self.conversion = conversion

        self.logger.debug(f"FFmpeg command : {command}")
        self.logger.debug(subprocess.Popen(command, shell=True))

        # Async conversion wizardry --------------------------------------

        self.conv_process = subprocess.Popen(
            command,
            shell=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            text=True,
        )

        GLib.io_add_watch(
            self.conv_process.stderr,
            GLib.IO_IN | GLib.IO_HUP,
            self.__convert_output,
        )

        GLib.child_watch_add(self.conv_process.pid, self.__convert_end)

        # ----------------------------------------------------------------

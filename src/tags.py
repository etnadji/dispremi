# tags.py
#
# Copyright 2021 Étienne Nadji
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import subprocess

from .common import escaped

# ------------------------------------------------------------------------------

SETS = {
    "mp3": [
        "album",
        "composer",
        "genre",
        "copyright",
        "encoded_by",
        "title",
        "language",
        "artist",
        "album_artist",
        "performer",
        "disc",
        "publisher",
        "track",
        "encoder",
        "lyrics",
        "compilation",
        "date",
        "creation_time",
        "album-sort",
        "artist-sort",
        "title-sort",
        "album",
        "genre",
        "compilation",
        "title",
        "encoded_by",
        "artist",
        "album_artist",
        "performer",
        "track",
    ],
    "ogg": [],
    "mp4": [
        "title",
        "author",
        "album_artist",
        "album",
        "grouping",
        "composer",
        "year",
        "track",
        "comment",
        "genre",
        "copyright",
        "description",
        "synopsis",
        "show",
        "episode_id",
        "network",
        "lyrics",
    ],
}


class TagsSet:
    def __init__(self, **kwargs):
        self.input_set = None
        self.output_set = None

        self.tags = self.__init_tags()

        if "conversion" in kwargs:
            self.from_conversion(kwargs["conversion"])

    def __init_tags(self):
        self.tags = {}

        # Create the sets

        for set_key, set_tags in SETS.items():
            # Create the set
            self.tags[set_key] = {}
            # Fill tags set with empty tags
            for tag in set_tags:
                self.tags[set_key][tag] = None

    def from_conversion(self, conversion):

        self.output_set = conversion.preset.audio["tags"]["set"]

        if self.output_set is None or conversion.input is None:
            return False

        # Run ffprobe against conversion source --------------------------

        command = " ".join(
            [
                "ffprobe",
                "-show_format",
                "-print_format",
                "json",
                escaped(conversion.input, "double"),
                "-loglevel",
                "quiet",
            ]
        )

        output = subprocess.getoutput(command)

        if not output:
            return False

        probed = json.loads(output)["format"]

        if "tags" not in probed:
            return False

        if "format_name" not in probed:
            return False

        # Extract datas --------------------------------------------------

        # 'tags': {
        #    'COMPATIBLE_BRANDS': 'iso6av01mp41', 'MAJOR_BRAND': 'dash',
        #    'MINOR_VERSION': '0', 'ENCODER': 'Lavf58.45.100'
        #  }
        input_tags = probed["tags"]

        # 'format_name': 'matroska,webm',
        # 'format_long_name': 'Matroska / WebM',
        input_formats = probed["format_name"].split(",")

        print("Probed")
        print(input_tags)

        valid_set = False

        for input_format in input_formats:
            if input_format in SETS.values():
                valid_set = input_format
                break

        print(input_formats, "=>", valid_set)

        if not valid_set:
            return False

        # Save datas -----------------------------------------------------

        self.input_set = valid_set

        for tag, tag_value in input_tags.items():
            if tag in SETS[valid_set]:
                self.tags[valid_set][tag] = tag_value

        return True

    def get_metadatas(self):
        # TODO Match input tag to output tags, making a list of
        # metadata name + value

        if self.input_set == self.output_set:
            metadatas = [
                [tag_name, str(tag_value)]
                for tag_name, tag_value in self.tags[self.input_set].items()
            ]
        else:
            metadatas = []
            # Here is the more complicated part
            pass

        return metadatas

    def get_command(self):
        # https://write.corbpie.com/adding-metadata-to-a-video-or-audio-file-with-ffmpeg/

        if self.output_set is None or self.input_set is None:
            return []

        metadatas = self.get_metadatas()

        command = []

        for meta_name, meta_value in metadatas:
            escaped_value = escaped(meta_value, "single")
            meta = escaped(f"{meta_name}={escaped_value}", "double")

            command += ["-metadata", meta]

        if not command:
            return command

        return ["-write_id3v2", "1"] + command

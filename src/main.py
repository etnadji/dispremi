# main.py
#
# Copyright 2021 Étienne Nadji
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# ------------------------------------------------------------------------------

# Standard library
import os
import sys
import logging
from pathlib import Path

# pygi
import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gio

# appdirs
from appdirs import user_config_dir

# Dispremi
from .window import DispremiWindow
from .settings import SettingsFile
from .translation import gettext as _

# ------------------------------------------------------------------------------


class Application(Gtk.Application):
    """
    Dispremi application.
    """

    def __init__(self):
        super().__init__(
            # Do not modify this.
            application_id="fr.etnadji.dispremi",
            # Necessary for Dispremi to open files from files explorers
            flags=Gio.ApplicationFlags.HANDLES_OPEN,
        )

        # Callbacks

        self.connect("open", self.__file_open)

    def _load_actions(self):
        """
        Load all Dispremi actions as GSimpleAction instances.
        """

        for action_name, callback in [
            ["open-converted", self.win.open_converted_file],
            ["see-converted", self.win.see_converted_file],
            ["show-settings", self.win.settings],
        ]:

            action = Gio.SimpleAction.new(f"app.{action_name}", None)
            action.connect("activate", callback)
            self.add_action(action)

    def __get_window(self, filepath=False):
        win = self.props.active_window

        if not win:
            win = DispremiWindow(application=self)

        # Was Dispremi started as a file opener ? ------------------------------

        if filepath:
            # Dispremi only converts one file at a time, so we only take the
            # first file in the list.
            filepath = filepath[0].get_path()
            win.init_cli(filepath)
        else:
            win.init_cli()

        # Initialize Dispremi configuration ------------------------------------

        config_dir = Path(user_config_dir())
        config_file = config_dir / "dispremi.json"

        # Make the config folder if it doenn't exists

        try:
            os.makedirs(str(config_dir))
        except FileExistsError:
            pass

        # Create or load Dispremi configuration --------------------------------

        if config_file.exists():
            config = SettingsFile(filepath=config_file)
        else:
            config = SettingsFile(filepath=config_file, new=True)

        # Create log file ------------------------------------------------------

        log_file = config_dir / "dispremi.log"

        try:
            os.remove(str(log_file))
        except:
            pass

        logger = logging.getLogger("Dispremi")
        logger.setLevel(logging.DEBUG)
        loghandler = logging.FileHandler(str(log_file))
        loghandler.setLevel(logging.DEBUG)
        formatter = logging.Formatter(
            "%(asctime)s - %(levelname)s - %(message)s"
        )
        loghandler.setFormatter(formatter)
        logger.addHandler(loghandler)

        # Initialize Dispremi window with configuration ------------------------

        win.init_config(config, logger)

        # ----------------------------------------------------------------------

        return win

    def __file_open(self, application, files, files_number, hint):
        """
        Create the application window when Dispremi was started as file opener.

        :type files: list
        """

        self.win = self.__get_window(files)
        self._load_actions()
        self.win.present()

    def do_notification(self, title, body, notification_id="0", actions=[]):
        """
        Create and send Gio Notification through Gtk.Application.

        :param title: Notification title
        :type title: str
        :param notification_id: Notification ID
        :type notification_id: str
        :param actions: List of lists including action label and action name
            without 'app.' prefix. See also `Application._load_actions`.
        :type actions: list
        """

        notification = Gio.Notification()
        notification.set_title(title)
        notification.set_body(body)

        for label, name in actions:
            # [action label, action name]
            notification.add_button(label, f"app.{name}")

        self.send_notification(notification_id, notification)

    def do_activate(self):
        """
        Create the application window in most cases except file opening.
        """

        self.win = self.__get_window()
        self._load_actions()
        self.win.present()


# ------------------------------------------------------------------------------


def main(version):
    """
    Dispremi startup function.
    """

    app = Application()
    return app.run(sys.argv)

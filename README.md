# Dispremi

<p align="center">
<a href="http://www.gnu.org/licenses/gpl-3.0"><img alt="License: GPL v3" src="https://img.shields.io/badge/License-GPL%20v3-blue.svg"></a> 
<a href="https://github.com/psf/black"><img alt="Code style: black" src="https://img.shields.io/badge/code%20style-black-000000.svg"></a>
</p>

Dispremi is a one-file-at-a-time simple video converter / reducer for GNOME,
meant to be usable by your grandmother.

It is a FFmpeg GUI with straightforward presets. Just select your options
if necessary, select your video file and wait.

![Screenshot](doc/screenshot.png)

## Credits

- The code for i18n is from the [Portfolio](https://github.com/tchx84/Portfolio) application.
- Dispremi's icon is a (poorly) modified [Celluloid](https://celluloid-player.github.io/) icon.

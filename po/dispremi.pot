# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-08-02 11:28+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Application menu item : settings
#: src/window.ui:38
msgctxt "Application menu item : settings"
msgid "Settings"
msgstr ""

#. Application menu item : about the application
#: src/window.ui:47
msgctxt "Application menu item : about the application"
msgid "About"
msgstr ""

#. Application menu item : quit the application
#: src/window.ui:55
msgctxt "Application menu item : quit the application"
msgid "Quit"
msgstr ""

#. Select a video to convert
#: src/window.ui:151
msgid "Select a video to convert"
msgstr ""

#. "Convert" button
#: src/window.ui:191
msgid "Convert"
msgstr ""

#. Keep/Remove audio checkbox
#: src/window.ui:247 src/settings.ui:192 src/preset.ui:113 src/preset.ui:218
#: src/preset.ui:430
msgid "Audio"
msgstr ""

#. Keep/Remove video checkbox
#: src/window.ui:262 src/settings.ui:206 src/preset.ui:139 src/preset.ui:232
#: src/preset.ui:521
msgid "Video"
msgstr ""

#. Invisible page title
#: src/window.ui:312
msgid "Welcome"
msgstr ""

#. Converting...
#: src/window.ui:346
msgctxt "Visible text in conversion page"
msgid "Converting…"
msgstr ""

#. Invisible page title
#: src/window.ui:397
msgid "Converting..."
msgstr ""

#. Conversion finished
#: src/window.ui:428
msgid "Nice !"
msgstr ""

#. Conversion finished
#: src/window.ui:444
msgid "Conversion terminée."
msgstr ""

#. Do a new conversion
#: src/window.ui:460 src/window.ui:636
msgid "New conversion"
msgstr ""

#. See the converted file [folder]
#: src/window.ui:473
msgid "Open the folder"
msgstr ""

#. See the converted file [folder]
#: src/window.ui:489 src/window.py:415
msgid "Play"
msgstr ""

#. Conversion finished
#: src/window.ui:520
msgid "... or continue with :"
msgstr ""

#. Invisible page title
#: src/window.ui:573
msgid "Conversion done"
msgstr ""

#. Conversion finished
#: src/window.ui:604
msgid "Crap !"
msgstr ""

#. Conversion finished
#: src/window.ui:620
msgid "The conversion failed…"
msgstr ""

#. Invisible page title
#: src/window.ui:682
msgid "Conversion failed"
msgstr ""

#: src/settings.ui:45
msgid "Annuler"
msgstr ""

#. OK button for settings dialog
#: src/settings.ui:58
msgctxt "OK button for settings dialog"
msgid "OK"
msgstr ""

#. Profile [used by default at application startup]
#: src/settings.ui:129
msgctxt "Label header in settings dialog"
msgid "Profile"
msgstr ""

#. Profile [used by default at application startup]
#: src/settings.ui:173 src/preset.ui:199
msgctxt "Label header in settings dialog"
msgid "Keep"
msgstr ""

#. Title of the stack item for default conversion settings at application startup
#: src/settings.ui:238
msgctxt "Settings : Title of a tab / stack item"
msgid "Default settings"
msgstr ""

#: src/settings.ui:258
msgid "Open converted file with default application"
msgstr ""

#: src/settings.ui:275
msgctxt "Entry temporary text"
msgid "Application command. Ex : vlc"
msgstr ""

#: src/settings.ui:308
msgid "Place converted files in"
msgstr ""

#: src/settings.ui:323
msgid "A single folder"
msgstr ""

#: src/settings.ui:324
msgctxt "Combox text"
msgid "The source folder"
msgstr ""

#: src/settings.ui:366
msgid "Folders & Files"
msgstr ""

#: src/settings.ui:396
msgid "Reset to Dispremi default presets"
msgstr ""

#. New profile button
#: src/settings.ui:434
msgid "New"
msgstr ""

#. Duplicate [current] profile button
#: src/settings.ui:447
msgctxt "Duplicate"
msgid "Duplicate"
msgstr ""

#. Delete preset button
#: src/settings.ui:460
msgid "Delete"
msgstr ""

#: src/settings.ui:495 src/settings.py:511
msgid "Import a preset"
msgstr ""

#: src/settings.ui:519
msgid "Export the selected preset"
msgstr ""

#: src/settings.ui:559
msgid "Move this preset before the preceding."
msgstr ""

#: src/settings.ui:583
msgid "Move this preset after the following."
msgstr ""

#. Title of the stack item for [FFmpeg] presets settings
#: src/settings.ui:693
msgctxt "Settings : Title of a tab / stack item"
msgid "Presets"
msgstr ""

#: src/preset.ui:52
msgid "Name"
msgstr ""

#: src/preset.ui:67
msgid "Name of the preset"
msgstr ""

#: src/preset.ui:92
msgid "File extensions"
msgstr ""

#: src/preset.ui:125 src/preset.ui:126 src/preset.ui:151 src/preset.ui:152
msgid "File extension without point"
msgstr ""

#: src/preset.ui:178
msgid "Default settings"
msgstr ""

#. Profile [used by default at application startup]
#: src/preset.ui:249
msgctxt "Label header in settings dialog"
msgid "Override"
msgstr ""

#: src/preset.ui:264
msgid "Wether the preset should override user audio / video choices"
msgstr ""

#: src/preset.ui:323
msgid "Frequency"
msgstr ""

#: src/preset.ui:336
msgid "Channels"
msgstr ""

#: src/preset.ui:349
msgid "Bitrate"
msgstr ""

#: src/preset.ui:375
msgid "Empty to leave frequency as it is"
msgstr ""

#: src/preset.ui:388
msgid "Empty to leave channels as they are"
msgstr ""

#: src/preset.ui:401
msgid "Empty to leave bitrate as it is"
msgstr ""

#: src/preset.ui:461
msgid "Filter"
msgstr ""

#: src/preset.ui:493
msgctxt "Entry tooltip"
msgid "Comma separated FFmpeg arguments"
msgstr ""

#: src/preset.ui:494
msgid "Empty OR FFmpeg video filters separated by commas"
msgstr ""

#: src/preset.ui:545 src/preset.ui:557
msgid "Preset ID"
msgstr ""

#: src/preset.ui:570
msgid "Quality indication"
msgstr ""

#: src/preset.ui:597
msgid "Preset details"
msgstr ""

#: src/preset.ui:637
msgid "Generated FFmpeg commands"
msgstr ""

#: src/preset.ui:653
msgid "Advanced default settings"
msgstr ""

#: src/settings.py:440
msgid "Restoring presets to Dispremi defaults"
msgstr ""

#: src/settings.py:442
msgid ""
"Are you sure you want to reset your presets to those defined by Dispremi?"
msgstr ""

#: src/settings.py:459
msgid "Deleting this preset"
msgstr ""

#: src/settings.py:461
msgid "Are you sure you want to delete the "
msgstr ""

#: src/settings.py:500
msgid "Presets (*.json)"
msgstr ""

#: src/settings.py:504
msgid "Any files"
msgstr ""

#: src/settings.py:552
msgid "Export a preset"
msgstr ""

#: src/settings.py:608
msgid "Single folder for converted files"
msgstr ""

#: src/settings.py:610
msgid ""
"The single folder for converted files has not been set. Please set it or "
"revert to the usage of the same folder than the source."
msgstr ""

#: src/settings.py:665
msgid "New preset"
msgstr ""

#: src/settings.py:697
msgid "Untitled preset"
msgstr ""

#: src/settings.py:697
msgid "A preset has no title."
msgstr ""

#: src/settings.py:706
msgid "Preset without file extensions"
msgstr ""

#: src/settings.py:708
#, python-brace-format
msgid ""
"The preset {widget.preset.title} doesn't define any extension for the "
"conversion output files."
msgstr ""

#: src/window.py:174
msgid ""
"Simple one-file FFmpeg convertor.\n"
"\n"
"Thanks to all FFmpeg contributors !"
msgstr ""

#: src/window.py:288
msgid "The file to convert doesn't exists."
msgstr ""

#: src/window.py:293
msgid "There is nothing to convert."
msgstr ""

#: src/window.py:295
msgid "You must keep at least the audio or the video in the converted file."
msgstr ""

#: src/window.py:301
msgid "The output file is not defined."
msgstr ""

#: src/window.py:303
msgid "This is not normal. Please report this as a bug."
msgstr ""

#: src/window.py:412
msgid "Conversion finished"
msgstr ""

#: src/window.py:413
#, python-brace-format
msgid "{output} is ready !"
msgstr ""

#: src/window.py:416
msgid "Folder"
msgstr ""

#: src/presets.py:158
msgid "copy"
msgstr ""

#: src/presets.py:429
msgid "High"
msgstr ""

#: src/presets.py:430
msgid "Medium"
msgstr ""

#: src/presets.py:431
msgid "Low"
msgstr ""

#: src/presets.py:471
msgid "haute qualité"
msgstr ""

#: src/presets.py:472
msgid "moyenne qualité"
msgstr ""

#: src/presets.py:473
msgid "basse qualité"
msgstr ""

#!/bin/bash
cd po

xgettext --files-from=POTFILES --directory=.. --output=dispremi.pot

msgmerge --update --no-fuzzy-matching --backup=off en.po dispremi.pot
msgmerge --update --no-fuzzy-matching --backup=off fr.po dispremi.pot
msgmerge --update --no-fuzzy-matching --backup=off eo.po dispremi.pot

# Changelog

## 2021-08-07

[47978c550432f038d164df751d3cca3a671857f9] **Settings** – 
Ability to change the presets order.

## 2021-08-02

[d9fd43c3c3b6b6858a180ae4828fc695b84d4e85] **i18n** – 
Better esperanto translation.

[9a81ab3b22255661f370c97d5599b8bfbc96a754] **Settings** – 
Dispremi can convert to a single user-defined folder instead of using 
the same folder as the source file.

[4b7a6282b69d3ffe57c3018bf269b16e79b91e09] **UI** – 
Default application moved in new “Folder &amp; Files” tab.

[c01de8c711f900a7e5535f479f4b3a05a6d19a02] **UI** – 
Bigger welcome message.

[450e9066eb64edeb8c5e741a9199e8735fc86588] **UI** – 
Symbolic icon.

[70c75420eab4dfdebbd9eca80c080500e39c0eb5] **Code** – 
Some bugfixes.

## 2021-07-28

[59fa6f379ee895eb6ee52bbd37f10d9c333b6cd2] **Code** - 
Improved file chooser dialogs.

## 2021-07-26

First public version.
